import csgo from "../data/images/games/csgo.jpg";
import rimworld from "../data/images/games/rimworld.jpg";
import ksp from "../data/images/games/ksp.jpg";
import msfs from "../data/images/games/msfs.jpg";
import assetto from "../data/images/games/assetto.jpg";

import snowfall from "../data/images/movies/snowfall.jpg";
import americanGangster from "../data/images/movies/american-gangster.jpg";
import boondocks from "../data/images/movies/boondocks.jpg";
import memento from "../data/images/movies/memento.jpg";
import intouchables from "../data/images/movies/intouchables.jpg";
import twelveMonkeys from "../data/images/movies/twelve-monkeys.jpg";

export const recommendations = [
    {
        type: "game",
        title: "RimWorld",
        img: rimworld,
        developer: "Ludeon Studios",
        releaseDate: 2018,
        playtime: 4350,
        stars: 4.5,
        summary: "A sci-fi colony sim driven by an intelligent AI storyteller, best in its genre.",
    },
    {
        type: "game",
        title: "Counter-Strike: Global Offensive",
        img: csgo,
        developer: "Valve",
        releaseDate: 2012,
        playtime: 70580,
        stars: 5,
        summary: "One of the only compentitive games I spent thousands of hours playing.",
    },
    {
        type: "game",
        title: "Kerbal Space Program",
        img: ksp,
        developer: "Squad",
        releaseDate: 2015,
        playtime: 16716,
        stars: 5,
        summary:
            "You design a mission, implement it, and on your way back you notice that you forgot heatshield and you burn up in atmosphere.",
    },
    {
        type: "game",
        title: "Assetto Corsa",
        img: assetto,
        developer: "Kunos Simulazioni",
        releaseDate: 2014,
        playtime: 3444,
        stars: 4.5,
        summary: "One of the best driving sims out there.",
    },
    {
        type: "game",
        title: "Microsoft Flight Simulator",
        img: msfs,
        developer: "Asobo",
        releaseDate: 2020,
        playtime: 92094,
        stars: 5,
        summary: "Totally worth it for someone interested in aviation.",
    },
    {
        type: "movie",
        title: "American Gangster",
        img: americanGangster,
        developer: "Ridley Scott",
        releaseDate: 2007,
        playtime: 176,
        stars: 4.2,
        summary:
            "Harlem drug dealer Frank Lucas rises to power in corrupt 1970s New York, equalling and surpassing the notorious Mafia families with the reach of his empire.",
    },
    {
        type: "show",
        title: "Snowfall",
        img: snowfall,
        developer: "FX",
        releaseDate: 2017,
        playtime: 6,
        stars: 5,
        summary:
            "A look at the early days of the crack cocaine epidemic in Los Angeles during the beginning of the 1980s. Young street dealer Franklin Saint wants to break away from the reefer game to make real money and begins peddling cocaine.",
    },
    {
        type: "show",
        title: "The Boondocks",
        img: boondocks,
        developer: "FX",
        releaseDate: 2005,
        playtime: 4,
        stars: 4,
        summary:
            "Adventures of two boys, Riley and Huey Freeman, who undergo a culture clash when they move from Chicago to the suburbs to live with their grandfather.",
    },
    {
        type: "movie",
        title: "Memento",
        img: memento,
        developer: "Christopher Nolan",
        releaseDate: 2000,
        playtime: 113,
        stars: 4,
        summary:
            "An insurance investigator who can't create new memories uses notes and tattoos to hunt for the man he thinks killed his wife, which is the last thing he remembers.",
    },
    {
        type: "movie",
        title: "Intouchables",
        img: intouchables,
        developer: "Olivier Nakache",
        releaseDate: 2011,
        playtime: 113,
        stars: 4.4,
        summary:
            "An unusual friendship develops when Driss, a street smart immigrant, is hired to take care of Philippe, a quadriplegic French nobleman.",
    },
    {
        type: "movie",
        title: "12 Monkeys",
        img: twelveMonkeys,
        developer: "Terry Gilliam",
        releaseDate: 1995,
        playtime: 125,
        stars: 4.5,
        summary:
            "Imprisoned in the 2030s, James Cole is recruited for a mission that will send him back to the 1990s. Once there, he's supposed to gather information about a nascent plague that's about to exterminate the vast majority of the world's population. ",
    },
];

export type RecordType = {
    type: "movie" | "show" | "game";
    title: string;
    img: string;
    developer: string;
    releaseDate: number;
    playtime: number;
    stars: number;
    summary: string;
};
